// ==UserScript==
// @name               IITC plugin: Alt Shards List
// @id                     iitc-plugin-alt-shards-list
// @updateURL     https://gitlab.com/ynakata/iitc-alt-shards-list/raw/master/alt-shards-list.user.js
// @downloadURL https://gitlab.com/ynakata/iitc-alt-shards-list/raw/master/alt-shards-list.user.js
// @version            0.1.6.20230820.194841
// @namespace     https://github.com/jonatkins/ingress-intel-total-conversion
// @description     show shards/targets list with statically order.
// @author             kipoo (Google+ : 中田吉法)
// @match              https://*.ingress.com/intel*
// @match              http://*.ingress.com/intel*
// @match              https://intel.ingress.com/*
// @match              http://intel.ingress.com/*
// @match              https://*.ingress.com/mission/*
// @match              http://*.ingress.com/mission/*
// @grant                none
// ==/UserScript==
/* jshint -W097 */
/* jshint multistr:true */
'use strict';

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

//PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
//(leaving them in place might break the 'About IITC' page or break update checks)
/*
plugin_info.buildName = 'jonatkins';
plugin_info.dateTimeVersion = '20230820.194841';
plugin_info.pluginId = 'alt-shards-list';
*/
//END PLUGIN AUTHORS NOTE

// PLUGIN START ////////////////////////////////////////////////////////
//setting
// set 'true' is danger :
//    It's automatically fetch details of portal, it perhaps causes BAN.
var autoFetchDetail = true;
var poralClassify = true;

// use own namespace for plugin
window.plugin.altShardsList = function() {};

// define constant value
const GoalCountLimit = 5;

window.plugin.altShardsList.loadAllDetails = function() {
    $.each(artifact.portalInfo, function(guid, data) {
      for (var key in data) {
        if (key.match(/(?:abaddon\d+|shard2017|shard-bravo|targetenl|targetres)/) && data[key].fragments) {
            window.portalDetail.request(guid);
        }
      }
    });
};

window.plugin.altShardsList.copyCsv = function() {
    var input = document.getElementById('plugin-alt-shards-list-csv');
    input.select();
    document.execCommand("copy");
}

window.plugin.altShardsList.showPortal = function(guid, latlng, zoom) {
  map.setView(latlng, zoom);
  // if the data is available, render it immediately. Otherwise defer
  // until it becomes available.
  if(window.portals[guid]) {
    renderPortalDetails(guid);
  } else {
    urlPortal = guid;
  }
}

window.plugin.altShardsList.artifactDetail = function(guid) {
  if (guid && autoFetchDetail && !window.portalDetail.isFresh(guid)) {
    window.portalDetail.request(guid);
  }
  var details = window.portalDetail.get(guid);
  if (details) {
    var data = getPortalSummaryData(details);
    var artifactClass = window.plugin.altShardsList.artifactClass(guid);
    var d = details;
    if (d.artifactDetail) {
      //debugger;
      var fragments = '';
      var colors = ['white','darkgray','lightcoral','firebrick','lightyellow','salmon','orangered','chocolate','darkorange','pink','gold','khaki','violet'];
      for (var i in d.artifactDetail.fragments){
          var val = d.artifactDetail.fragments[i];
          var color_index = Math.floor( (val - 1) /5);
          var style = ' style="color: ' + colors[color_index]+'" ';
          fragments = fragments + '<span '+style+'>#'+val+'</span>';
      }
      return '('+ d.artifactDetail.fragments.length +')'+ fragments;
    }
  } else {
    return null;
  }
};

window.plugin.altShardsList.artifactCount = function(guid) {
    var details = window.portalDetail.get(guid);
    if (details) {
        var data = getPortalSummaryData(details);
        var artifactClass = window.plugin.altShardsList.artifactClass(guid);
        var d = details;
        if (d.artifactDetail) {
            return d.artifactDetail.fragments.length;
        }
    }
    return 0;
}

window.plugin.altShardsList.artifactDetailCsv = function(guid) {
    if(guid && autoFetchDetail && !window.portalDetail.isFresh(guid)){
        window.portalDetail.request(guid);
    }
    var details = window.portalDetail.get(guid);
    if(details){
        var data = getPortalSummaryData(details);
        var artifactClass = window.plugin.altShardsList.artifactClass(guid);
        var d = details;
        if(d.artifactDetail){
            var fragments = '';
            for (var i in d.artifactDetail.fragments){
                var val = d.artifactDetail.fragments[i];
                fragments += '#'+val;
            }
            return window.plugin.altShardsList.formCsv(d.title, fragments, d.latE6, d.lngE6);
        }
    }
}

window.plugin.altShardsList.fragmentsOnPortal = function(guid) {
    if(guid && autoFetchDetail && !window.portalDetail.isFresh(guid)){
        window.portalDetail.request(guid);
    }
    var details = window.portalDetail.get(guid);
    if(details){
        var data = getPortalSummaryData(details);
        var artifactClass = window.plugin.altShardsList.artifactClass(guid);
        var d = details;
        if(d.artifactDetail){
            var fragments = '';
            for (var i in d.artifactDetail.fragments){
                var val = d.artifactDetail.fragments[i];
                fragments += '#'+val;
            }
            return fragments;
        }
    }
}


window.plugin.altShardsList.formCsv = function(title, fragments, latE6, lngE6) {
    if (latE6 === undefined) {
        return title + ',' + fragments + ','
    }
    return title + ',' + fragments + ',' + ((latE6 !== undefined)? latE6/1E6 : '') + ',' + ((lngE6 !== undefined) ? lngE6/1E6 : '');
}

window.plugin.altShardsList.artifactClass = function(guid) {
  var details = window.portalDetail.get(guid);
  var colors = ['white','darkgray','lightcoral','firebrick','lightyellow','salmon','orangered','chocolate','darkorange','pink','gold','khaki','violet'];
  if (details) {
    data = getPortalSummaryData(details);
    var d = details;
    if (d.artifactDetail) {
        var fragments = d.artifactDetail.fragments[0];
        var color_index = Math.floor( (fragments-1) / 7);
        return ' style="color: '+colors[color_index]+'" ';
    }
  } else {
    return null;
  }
};

var isShowing = false;
window.addHook('portalDetailLoaded',function(data){
    if (isShowing) {
          window.plugin.altShardsList.showWindow();
    }
});

var firstLoad = false;

window.plugin.altShardsList.showWindow = function() {
  var csv = '';
  var html = '';
  var enlGoals = 0;
  var resGoals = 0;
  var freeShards = 0;
  var artifact = window.artifact;
  if (Object.keys(artifact.artifactTypes).length == 0) {
    html += '<i>No artifacts at this time</i>';
  }

  var first = true;
  var artifactTypePatterns = [/(abaddon|target)res/, /(abaddon|target)enl/, /abaddon\d+/, /shard(2017|-bravo)/];
  var artifactTypes = artifactTypePatterns.map(function(pattern) {
    return Object.keys(artifact.artifactTypes).find(function(value) { return value.match(pattern); } );
  });
  //debugger;
  var keyOfShardInfo = artifactTypes[2];
  if (keyOfShardInfo == undefined) {
      keyOfShardInfo = artifactTypes[3];
  }
  artifactTypes = artifactTypes.filter(function(value) { return value !== undefined; });

  var processedGuids = [];

  $.each(artifactTypes, function(index, type) {
    type2 = artifact.artifactTypes[type];

      var name = type.replace(keyOfShardInfo,'shards').replace('abaddon','').replace('target', '').replace('enl', 'enl target').replace('res', 'res target').capitalize();

    if (first){
        html += '<table class="artifact">';
        html += '<tr><th>Type</th><th>Portal</th><th>hasShards</th></tr>';
    }
    first = false;

    var tableRows = [];
    $.each(artifact.portalInfo, function(guid, data) {
      if ($.inArray(guid, processedGuids) >= 0) {
          return;
      }
      if (type in data) {
        processedGuids.push(guid);
        // this portal has data for this artifact type - add it to the table
        var whichTarget = '';
        if (data.abaddonres !== undefined && data.abaddonres.target !== undefined) {
            whichTarget = 'res';
        }
        if (data.targetres !== undefined && data.targetres.target !== undefined) {
            whichTarget = 'res';
        }
        if (data.abaddonenl !== undefined && data.abaddonenl.target !== undefined) {
            whichTarget = 'enl';
        }
        if (data.targetenl !== undefined && data.targetenl.target !== undefined) {
            whichTarget = 'enl';
        }

        var hasShards = '';
        var artifactDetail = '';
        var artifactClass = '';
        var artifactDetailCsv = '';
        if (data[keyOfShardInfo] !== undefined && data[keyOfShardInfo].fragments) {
            artifactDetail = window.plugin.altShardsList.artifactDetail(guid);
            artifactClass = window.plugin.altShardsList.artifactClass(guid);
            var artifactCount = window.plugin.altShardsList.artifactCount(guid);
            if(whichTarget === 'enl'){
                enlGoals += (artifactCount > GoalCountLimit) ? GoalCountLimit : artifactCount;
            } else if (whichTarget === 'res') {
                resGoals += (artifactCount > GoalCountLimit) ? GoalCountLimit : artifactCount;
            } else {
                freeShards += artifactCount;
            }

            hasShards = artifactDetail ? artifactDetail : 'yes (detail not fetch yet)';

            artifactDetailCsv = window.plugin.altShardsList.artifactDetailCsv(guid);
            artifactDetailCsv = artifactDetailCsv ? artifactDetailCsv : 'yes (no detail)';
        } else {
            var d = data._data;
            artifactDetailCsv = window.plugin.altShardsList.formCsv(d.title, '', d.latE6, d.lngE6);
        }

        //var onclick = 'zoomToAndShowPortal(\''+guid+'\',['+data._data.latE6/1E6+','+data._data.lngE6/1E6+'])';
        var onclick = 'window.plugin.altShardsList.showPortal(\''+guid+'\',['+data._data.latE6/1E6+','+data._data.lngE6/1E6+'])';
        var row = '<tr><td class="artifact artifact-type '+whichTarget+'">'+whichTarget+'</td><td class="portal"><a onclick="'+onclick+'">'+escapeHtmlSpecialChars(data._data.title)+'</a></td>';
        var rowCsv = whichTarget+','+guid+','+ artifactDetailCsv;

        row += '<td class="info">'+hasShards+'</td></tr>';

        // sort by target portals first, then by portal GUID
        var sortVal = data._data.latE6 + (data[type].target !== undefined ? 'A' : 'Z') + guid;
        tableRows.push ( [sortVal, row, rowCsv] );
      }
    });

    // sort the rows
    tableRows.sort(function(a,b) {
      if (a[0] == b[0]) return 0;
      else if (a[0] < b[0]) return -1;
      else return 1;
    });

    // and add them to the table
    html += tableRows.map(function(a){return a[1];}).join('');
    csv += tableRows.map(function(a){return a[2];}).join('\n') + '\n';
  });

  if (!first) html += '</table>';

    html = '<div>E '+ enlGoals + ' - ' + resGoals + ' R  : notGoaled=' + freeShards + '</div>' + html;

    var date = new Date();
    html += '<div>'
        +'<textarea id="plugin-alt-shards-list-csv" rows="1" style="vertical-align:top; height:16px;">'+date.toUTCString()+'\n'+csv+'</textarea>'
        +'<button onclick="window.plugin.altShardsList.loadAllDetails()">load all details</button>'
        +'<button onclick="window.plugin.altShardsList.copyCsv()">copy csv</button>'
        +'</div>';

  window.dialog({
        html: html,
        width: '600px',
        id: 'plugin-alt-shards-list',
        dialogClass: 'ui-dialog-drawtoolsSet',
        title: 'alt shards list',
        resizable: true,
        close: function() {
            //debugger;
            isShowing = false;
        }
    });
    isShowing = true;
};

window.getPortalMiscDetails = function(guid,d) {

  var randDetails;

  if (d) {

    // collect some random data that’s not worth to put in an own method
    var linkInfo = getPortalLinks(guid);
    var maxOutgoing = getMaxOutgoingLinks(d);
    var linkCount = linkInfo.in.length + linkInfo.out.length;
    var links = {incoming: linkInfo.in.length, outgoing: linkInfo.out.length};

    var title = 'at most ' + maxOutgoing + ' outgoing links\n' +
                links.outgoing + ' links out\n' +
                links.incoming + ' links in\n' +
                '(' + (links.outgoing+links.incoming) + ' total)'
    var linksText = ['links', links.outgoing+' out / '+links.incoming+' in', title];

    var player = d.owner
      ? '<span class="nickname">' + d.owner + '</span>'
      : '-';
    var playerText = ['owner', player];


    var fieldCount = getPortalFieldsCount(guid);

    var fieldsText = ['fields', fieldCount];

    var apGainText = getAttackApGainText(d,fieldCount,linkCount);

    var attackValues = getPortalAttackValues(d);


    // collect and html-ify random data

    var randDetailsData = [
      // these pieces of data are only relevant when the portal is captured
      // maybe check if portal is captured and remove?
      // But this makes the info panel look rather empty for unclaimed portals
      playerText, getRangeText(d),
      linksText, fieldsText,
      getMitigationText(d,linkCount), getEnergyText(d),
      // and these have some use, even for uncaptured portals
      apGainText, getHackDetailsText(d),
    ];

    if(attackValues.attack_frequency != 0){
      randDetailsData.push([
        '<span title="attack frequency" class="text-overflow-ellipsis">attack frequency</span>',
        '×'+attackValues.attack_frequency]);
    }
    if(attackValues.hit_bonus != 0){
      randDetailsData.push(['hit bonus', attackValues.hit_bonus+'%']);
    }
    if(attackValues.force_amplifier != 0){
      randDetailsData.push([
        '<span title="force amplifier" class="text-overflow-ellipsis">force amplifier</span>',
        '×'+attackValues.force_amplifier]);
    }
    randDetails = '<table id="randdetails">' + genFourColumnTable(randDetailsData) + '</table>';


    // artifacts - tacked on after (but not as part of) the 'randdetails' table
    // instead of using the existing columns....

    if (d.artifactBrief && d.artifactBrief.target && Object.keys(d.artifactBrief.target).length > 0) {
      var targets = Object.keys(d.artifactBrief.target);
//currently (2015-07-10) we no longer know the team each target portal is for - so we'll just show the artifact type(s)
       randDetails += '<div id="artifact_target">Target portal: '+targets.map(function(x) { return x.capitalize(); }).join(', ')+'</div>';
    }

    // shards - taken directly from the portal details
    if (d.artifactDetail) {
        randDetails += '<div id="artifact_fragments">Shards: <span style="font-size:0pt">'+d.title+'</span> '+d.artifactDetail.displayName+'('+d.artifactDetail.fragments.length+') <span style="font-size:10pt">#'+d.artifactDetail.fragments.join(', ')+'</span> </div>';
    }
  }

  return randDetails;
}

window.plugin.altShardsList.filterType = function(type) {
    const re = /filter=([a-zA-Z]*)/g;
    var match = re.exec( location.search);
    if (match === null) return true;

    var pattern = new RegExp(match[1]);
    if(pattern.test(type)){
        return true;
    }
    return false;
}

//replace standard artifacts window's drawing
window.artifact.showArtifactList = function() {
  var html = '';

  if (Object.keys(artifact.artifactTypes).length == 0) {
    html += '<i>No artifacts at this time</i>';
  }

  var first = true;
  $.each(artifact.artifactTypes, function(type,type2) {
    // no nice way to convert the Niantic internal name into the correct display name
    // (we do get the description string once a portal with that shard type is selected - could cache that somewhere?)
    if(window.plugin.altShardsList.filterType(type) == false) return;

    var name = type.capitalize();

    if (!first) html += '<hr>';
    first = false;
    html += '<div><b style="font-size:14pt;">'+name+'</b></div>';

    html += '<table class="artifact artifact-'+type+'">';
    html += '<tr style="font-size:13pt;"><th>Portal</th><th>Details</th></tr>';

    var tableRows = [];

    $.each(artifact.portalInfo, function(guid, data) {
      if (type in data) {
        // this portal has data for this artifact type - add it to the table

        var onclick = 'window.plugin.altShardsList.showPortal(\''+guid+'\',['+data._data.latE6/1E6+','+data._data.lngE6/1E6+'])';
        var row = '<tr><td class="portal"><a onclick="'+onclick+'">'+escapeHtmlSpecialChars(data._data.title)+'</a></td>';

        row += '<td class="info">';

        if (data[type].target !== undefined) {
          if (data[type].target == TEAM_NONE) {
            row += '<span class="target">Target Portal</span> ';
          } else {
            row += '<span class="target '+TEAM_TO_CSS[data[type].target]+'">'+(data[type].target==TEAM_RES?'Resistance':'Enlightened')+' target</span> ';
          }
        }

        var sortKey = '';
        if (data[type].fragments) {
          if (data[type].target !== undefined) {
            row += '<br>';
          }
          var detail = window.plugin.altShardsList.fragmentsOnPortal(guid);
          detail = detail ? detail : 'shard: yes';
          sortKey = detail;
          row += '<span class="fragments'+(data[type].target?' '+TEAM_TO_CSS[data[type].target]:'')+'">'+ detail+'</span> ';
        }

        row += '</td></tr>';

        // sort by target portals first, then by portal GUID
        var sortVal = (data[type].target !== undefined ? 'A' : 'Z') + sortKey + guid;

        tableRows.push ( [sortVal, row] );
      }
    });

    // check for no rows, and add a note to the table instead
    if (tableRows.length == 0) {
      html += '<tr><td colspan="2"><i>No portals at this time</i></td></tr>';
    }

    // sort the rows
    tableRows.sort(function(a,b) {
      if (a[0] == b[0]) return 0;
      else if (a[0] < b[0]) return -1;
      else return 1;
    });

    // and add them to the table
    html += tableRows.map(function(a){return a[1];}).join('');


    html += '</table>';
  });

  // In Summer 2015, Niantic changed the data format for artifact portals. We no longer know:
  // - Which team each target portal is for - only that it is a target
  // - Which shards are at each portal, just that it has one or more shards
  // You can select a portal and the detailed data contains the list of shard numbers, but there's still no
  // more information on targets

  dialog({
    title: 'Artifacts',
    id: 'iitc-artifacts',
    html: html,
    width: 400,
    position: {my: 'right center', at: 'center-60 center', of: window, collision: 'fit'}
  });

}

var setup = function() {  $('#toolbox').append(' <a onclick="window.plugin.altShardsList.showWindow()" title="Alt Shards List">AltShards</a>');
};
// PLUGIN END //////////////////////////////////////////////////////////

setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
