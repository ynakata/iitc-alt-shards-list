AltShardsList
=============

IITC標準のShardsよりも使いやすいシャード一覧を提供する IITC Plugin です

配布について
------------

* Google Drive 及び gitlab.com の2つの経路で配布します
* 二次配布を禁止します
* Google Drive配布については、Google groups単位で配布許可を出します。ynwhite at gmail.com に申請してください。
* gitlab.com配布については、gitlab.comのアカウントを作成した上でynwhite at gmail.com に申請してください。

機密レベル
----------

* 本ツールの存在を積極的に公にする行為は禁止します。
* 機能自体は本家IITCの見た目改善相当ですので、それほど重大ではありませんが、青さんに知られるのは避けてください。

導入方法
--------

Google Drive配布の場合、Google Driveの制限によりクリックだけではインストールできません。
Tampermonkey / Greasemonkey のコンソールにスクリプト(alt-shards-list.user.js)の中身を貼り付ける形で導入してください。

gitlab.com配布の場合、ファイルをRaw形式で取得すれば Plugin Install の画面が表示されます。
  URL: <https://gitlab.com/ynakata/iitc-alt-shards-list/raw/master/alt-shards-list.user.js>
